# Changelog

## [2.0.0](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/compare/html-obfuscator-1.1.0...2.0.0) (2023-06-09)


### Documentation

* add CONTRIBUTING.md ([2e2296e](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/2e2296e7d5f8d7c20e4e1a222af838995f868dee))
* add LICENSE ([71d8e13](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/71d8e136636335f2f4586e94ecf7e58ab1d79cba))


### Other changes

* add a Netlify config file ([ab7f8e5](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/ab7f8e53b2cc05f639b1db9c4ff9ba2ac70ccc63))
* add Husky ([c007f5f](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/c007f5f128e8aff3e42ad3a29e4c6f220cb46eda))
* add Release It ([d25ffa3](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/d25ffa342cbdc898162bf465f791cc5dc4de83e3))
* add SonarQube integration ([b3744f5](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/b3744f55d1510081c0a3a0a0376d90c2655bd2fc))
* bump React to 18.2 ([09db43b](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/09db43b8073b3ed08c37c2b583fc8bceb15f4b9d))
* enforce code style with ESLint and Prettier ([26c982f](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/26c982fdc9658526c555f22ce89cb4f330beae6a))
* move the global reducer to a separate file ([85b7abe](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/85b7abe3ae3845b3d72483f8f4ff58d958a157bc))
* publish a test report using Gitlab Pages ([925136a](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/925136a0f596fb23de55105a8032e094e74a32c0))
* rename project files accordingly to the naming convention ([7b57936](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/7b5793676162acac2867fd1dad9d37560d60b89a))
* update browserslist ([8758c49](https://gitlab.com/ShowMeYourCodeYouTube/html-code-obfuscator/commit/8758c49af5084ed0cc4d179fcef6e288d5582d26))