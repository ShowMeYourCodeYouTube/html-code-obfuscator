import React, { Component } from 'react'
import './App.scss'
import { Col, Container, Nav, NavItem, NavLink, Row, TabContent, TabPane } from 'reactstrap'
import classnames from 'classnames'
import ConfigurationFormPage from './pages/ConfigFormPage/ConfigFormPage'
import HtmlPreviewPage from './pages/HtmlPreviewPage/HtmlPreviewPage'
import SettingsTabPage from './pages/SettingsTabPage/SettingsTabPage'
import ObfuscationOutputPage from './pages/ObfuscationOutputPage/ObfuscationOutputPage'
import { type ApplicationState, getInitialState } from './state/applicationState'
import { type Action } from './actions/action'
import packageJson from '../package.json'
import GlobalReducer from './reducers/globalReducer'
import { EMPTY_HTML } from './model/configs/obfuscationConfig'
import { LOGGER } from './utils/logging'

class App extends Component<Record<string, unknown>, ApplicationState> {
  private readonly reducer: GlobalReducer
  constructor(props: Record<string, unknown>) {
    super(props)
    this.reducer = new GlobalReducer()
    this.state = getInitialState()
  }

  componentDidMount(): void {
    LOGGER.info(`Program version: ${packageJson.version}`)
  }

  reduceAction(action: Action): void {
    this.setState(this.reducer.reduce(this.state, action))
  }

  render(): React.ReactNode {
    return (
      <Container>
        <div>
          <header>
            <h1>HTML obfuscator</h1>
          </header>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '1' })}
                onClick={() => {
                  this.toggle('1')
                }}
                href='#'
              >
                Configuration
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '2' })}
                onClick={(): void => {
                  this.toggle('2')
                }}
                href='#'
                disabled={this.state.obfuscationConfig.html === EMPTY_HTML}
              >
                HTML Preview
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '3' })}
                onClick={() => {
                  this.toggle('3')
                }}
                href='#'
                disabled={this.state.outputObfuscationConfig == null}
              >
                Result
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '4' })}
                onClick={() => {
                  this.toggle('4')
                }}
                href='#'
              >
                Settings
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId='1'>
              <Row>
                <Col>
                  <ConfigurationFormPage
                    algorithms={this.state.algorithms}
                    config={this.state.obfuscationConfig}
                    showHtmlTemplateModal={this.state.showHtmlTemplateModal}
                    callbackProcessAction={(e: Action) => {
                      this.reduceAction(e)
                    }}
                  />
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId='2'>
              <Row>
                <Col>
                  <HtmlPreviewPage
                    previewHtml={this.state.obfuscationConfig.html}
                    callbackProcessAction={(e: Action) => {
                      this.reduceAction(e)
                    }}
                    htmlConfig={this.state.obfuscationConfig.htmlConfig}
                  />
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId='3'>
              <Row>
                <Col>
                  <ObfuscationOutputPage
                    config={this.state.outputObfuscationConfig}
                    callbackProcessAction={(e: Action) => {
                      this.reduceAction(e)
                    }}
                  />
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId='4'>
              <Row>
                <Col>
                  <SettingsTabPage />
                </Col>
              </Row>
            </TabPane>
          </TabContent>
        </div>
      </Container>
    )
  }

  toggle(tab: string): void {
    if (this.state.activeTab !== tab) {
      this.setState({
        ...this.state,
        activeTab: tab,
      })
    }
  }
}

export default App
