import HtmlGeneratorFactory from '../factories/htmlGeneratorFactory'
import { type HtmlSection } from '../model/html'

export interface HtmlConfig {
  header: number
  input: number
  paragraph: number
  section: number
}

export class HtmlGeneratorService {
  private readonly htmlFactory: HtmlGeneratorFactory

  constructor() {
    this.htmlFactory = new HtmlGeneratorFactory()
  }

  generateHTML(config: HtmlConfig): string {
    let result = this.htmlFactory.getOpeningTag()
    let sectionNumber: number = config.section
    for (; sectionNumber > 0; sectionNumber--) {
      const sectionResult = this.generateSection(config)
      result += sectionResult.html
      config = sectionResult.config
    }
    result += this.htmlFactory.getClosingTag()

    return result
  }

  prepareHtmlConfig(
    sectionCount: number,
    headerCount: number,
    paragraphCount: number,
    inputCount: number,
  ): HtmlConfig {
    return {
      section: sectionCount,
      header: headerCount,
      paragraph: paragraphCount,
      input: inputCount,
    }
  }

  private generateSection(config: HtmlConfig): HtmlSection {
    const limit = 3
    let headerCount: number = (this.htmlFactory.randomCount() % limit) + 1
    let paragraphCount: number = (this.htmlFactory.randomCount() % limit) + 1
    let inputCount: number = (this.htmlFactory.randomCount() % limit) + 1

    const updatedConfig: HtmlConfig = {
      header: config.header - headerCount,
      paragraph: config.paragraph - paragraphCount,
      input: config.input - inputCount,
      section: 0,
    }

    headerCount = config.header - headerCount < 0 ? config.header : headerCount
    paragraphCount = config.paragraph - paragraphCount < 0 ? config.paragraph : paragraphCount
    inputCount = config.input - inputCount < 0 ? config.input : inputCount

    let isQuiz = false
    let isEmptySection = true

    let section = '<section>'
    for (; headerCount > 0; headerCount--) {
      isEmptySection = false
      let notEmpty = false
      section += this.htmlFactory.getRandomHeader()
      if (paragraphCount > 0) {
        notEmpty = true
        const withLink = Math.random() >= 0.5
        paragraphCount--
        if (withLink) {
          section += this.htmlFactory.getRandomParagraph(true)
        } else {
          section += this.htmlFactory.getRandomParagraph(false)
        }
      }
      if (Math.random() >= 0.5) {
        notEmpty = true
        section += this.htmlFactory.getRandomImage()
      }
      if (!notEmpty) {
        section += this.htmlFactory.getRandomBlockQuote()
      }
    }
    if (Math.random() >= 0.5 && inputCount > 0) {
      isEmptySection = false
      isQuiz = true
      section += this.htmlFactory.generateQuizForm(inputCount)
    }
    for (; paragraphCount > 0; paragraphCount--) {
      isEmptySection = false
      section += '</br>'
      if (Math.random() >= 0.5) {
        section += this.htmlFactory.getRandomBlockQuote()
      }
      const withLink = Math.random() >= 0.5
      section += this.htmlFactory.getRandomParagraph(withLink)
      if (Math.random() >= 0.5) {
        section += this.htmlFactory.getRandomImage()
      }
    }
    if (!isQuiz && inputCount > 0) {
      isEmptySection = false
      section += this.htmlFactory.generateQuizForm(inputCount)
    }

    if (isEmptySection) {
      section += this.htmlFactory.getRandomBlockQuote()
    }

    section += '</section>'

    return { html: section, config: updatedConfig }
  }
}
