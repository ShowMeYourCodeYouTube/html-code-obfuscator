import '@testing-library/jest-dom/extend-expect'
import HtmlEncoderService from './htmlEncoderService'

describe('HtmlEncoderService', () => {
  const htmlEncodeService: HtmlEncoderService = new HtmlEncoderService()
  const sampleHtml = `
    <html lang="en">
      <body>
        <h1>Hello</h1>
        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi in odio orci.</div>
      </body>
    </html>`

  test('it should decode and encode using an own function', () => {
    const encoded = htmlEncodeService.encodeUsingOwnFunction(sampleHtml)
    const decoded = htmlEncodeService.decodeUsingOwnFunction(encoded)

    expect(decoded).toContain(sampleHtml)
  })

  test('it should decode and encode using hex', () => {
    const encoded = htmlEncodeService.toHex(sampleHtml)
    const decoded = htmlEncodeService.fromHex(encoded)

    expect(decoded).toBe(sampleHtml)
  })

  test('it should decode and encode using hex', () => {
    const encoded = htmlEncodeService.toHex(sampleHtml)
    const decoded = htmlEncodeService.fromHex(encoded)

    expect(decoded).toBe(sampleHtml)
  })

  test('it should decode and encode using HTML entities', () => {
    const encoded = htmlEncodeService.toHtmlEntities(sampleHtml)
    const decoded = htmlEncodeService.fromHtmlEntities(encoded)

    expect(decoded).toBe(sampleHtml)
  })
})
