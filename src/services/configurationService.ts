import { type ObfuscationConfig } from '../model/configs/obfuscationConfig'
import { type PreservedConfiguration } from '../model/configs/preservedConfiguration'

export class ConfigurationService {
  static readonly APP_KEY = 'HTML_OBFUSCATOR_USER_CONFIG'

  checkIfConfigExists(name: string): boolean {
    return this.loadConfigs().reduce(
      (result: boolean, next: PreservedConfiguration) => next.name === name,
      false,
    )
  }

  clearUserConfigs(): void {
    localStorage.removeItem(ConfigurationService.APP_KEY)
  }

  getInitialObfuscationConfig(): ObfuscationConfig {
    return {
      html: '',
    }
  }

  loadConfigs(): PreservedConfiguration[] {
    let result = []
    const configs = localStorage.getItem(ConfigurationService.APP_KEY)
    if (configs != null) {
      result = JSON.parse(configs)
    }
    return result
  }

  saveConfig(userConfig: PreservedConfiguration): void {
    if (localStorage.getItem(ConfigurationService.APP_KEY) == null) {
      localStorage.setItem(ConfigurationService.APP_KEY, JSON.stringify([userConfig]))
    } else {
      const list = this.loadConfigs()
      list.push(userConfig)
      localStorage.setItem(ConfigurationService.APP_KEY, JSON.stringify(list))
    }
  }
}
