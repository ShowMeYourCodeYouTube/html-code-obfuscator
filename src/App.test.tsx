import React from 'react'
import App from './App'
import '@testing-library/jest-dom/extend-expect'
import { screen } from '@testing-library/dom'
import {
  act,
  findAllByText,
  findByTestId,
  findByText,
  fireEvent,
  render,
  type RenderResult,
} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { ElementId } from './model/elementId'
import { ConfigurationService } from './services/configurationService'
import ConfigurationFactory from './factories/configurationFactory'
import { HtmlFileType } from './model/htmlTypes'
import { AlgorithmType } from './model/algorithms/algorithmType'

jest.setTimeout(15000)

describe('App', () => {
  const settingsTabText = 'Settings'
  const configurationTabText = 'Configuration'
  const htmlPreviewTabText = 'HTML Preview'
  const resultTabText = 'Result'

  beforeEach(() => {
    localStorage.clear()
  })

  test('it should display nav links correctly when the app is opened for the first time', async () => {
    const page = await act(async () => render(<App />))

    expect(await screen.findByText('HTML obfuscator')).toBeVisible()

    const activeNavItems = page.container.getElementsByClassName('active nav-link')

    expect(activeNavItems.length).toBe(1)
    expect(activeNavItems[0].textContent).toBe(configurationTabText)

    const disabledNavItems = page.container.getElementsByClassName('nav-link disabled')

    expect(disabledNavItems.length).toBe(2)
    expect(disabledNavItems[0].textContent).toBe('HTML Preview')
    expect(disabledNavItems[1].textContent).toBe('Result')

    const allNavItems = page.container.getElementsByClassName('nav-link')
    expect(allNavItems.length).toBe(4)
  })

  test('it should open the settings tab and clear the local storage', async () => {
    localStorage.setItem(
      ConfigurationService.APP_KEY,
      JSON.stringify([
        {
          config: {
            html: '<html lang="en"> <body><h1>Hello World</h1></body> </html>',
          },
          name: 'TESTING-CONFIG',
        },
      ]),
    )
    const page = await act(async () => render(<App />))

    const activeNavItems = page.container.getElementsByClassName('active nav-link')

    expect(activeNavItems.length).toBe(1)
    expect(activeNavItems[0].textContent).toBe(configurationTabText)

    const user = userEvent.setup()
    await user.click(await findByText(page.container, settingsTabText))

    verifyActiveTab(page, settingsTabText)

    await user.click(await findByText(page.container, 'Clear'))

    expect(localStorage.getItem(ConfigurationService.APP_KEY)).toBeNull()
  })

  test('it should save a new configuration', async () => {
    expect(localStorage.getItem(ConfigurationService.APP_KEY)).toBeNull()
    const page = await act(async () => render(<App />))
    await loadHtmlEntitiesConfig(page)

    verifyActiveTab(page, configurationTabText)

    const user = userEvent.setup()
    await user.click(await findByText(page.container, 'Submit'))

    verifyActiveTab(page, resultTabText)

    fireEvent.change(await findByTestId(page.container, ElementId.RESULT_SAVE_CONFIG), {
      target: {
        value: 'test-config',
      },
    })

    await user.click(await findByText(page.container, 'Save configuration'))

    expect(localStorage.getItem(ConfigurationService.APP_KEY)).not.toBeNull()
  })

  test('it should load a configuration using predefined HTML entities', async () => {
    const page = await act(async () => render(<App />))

    await loadHtmlEntitiesConfig(page)
  })

  test('it should load a configuration using predefined Base64', async () => {
    const page = await act(async () => render(<App />))

    await loadPredefinedConfiguration(page, 1, 'Loris Cafe')
  })

  test('it should regenerate HTML using a predefined configuration', async () => {
    const predefinedHtmlTemplate = 'Company summary'
    const user = userEvent.setup()
    const page = await act(async () => render(<App />))

    await loadHtmlEntitiesConfig(page)

    const activeNavItems = page.container.getElementsByClassName('nav-link')
    expect(activeNavItems.length).toBe(4)

    await user.click(await findByText(page.container, htmlPreviewTabText))

    await user.click(await findByText(page.container, 'Generate again'))

    expect(screen.queryByText(predefinedHtmlTemplate)).not.toBeInTheDocument()
    expect((await findAllByText(page.container, 'Quiz!')).length > 0).toBeTruthy()
  })

  test('it should fill a configuration form from scratch', async () => {
    const page = await act(async () => render(<App />))

    fireEvent.change(
      await findByTestId(page.container, `${ElementId.GENERATE_ALGORITHM_OPTION_BTN}`),
      {
        target: {
          value: AlgorithmType.HTML_TO_JAVASCRIPT,
        },
      },
    )

    fireEvent.change(await findByTestId(page.container, ElementId.GENERATE_HTML_OPTION_BTN), {
      target: {
        value: HtmlFileType.GENERATE,
      },
    })

    const user = userEvent.setup()

    fireEvent.change(screen.getByTestId('Sections'), {
      target: {
        value: '2',
      },
    })

    fireEvent.change(screen.getByTestId('Headers'), {
      target: {
        value: '2',
      },
    })

    fireEvent.change(screen.getByTestId('Paragraphs'), {
      target: {
        value: '2',
      },
    })

    fireEvent.change(screen.getByTestId('Inputs'), {
      target: {
        value: '2',
      },
    })

    await user.click(await findByText(page.container, 'Generate'))

    await user.click(await findByTestId(page.container, ElementId.GENERATE_SUBMIT_BTN))
    verifyActiveTab(page, resultTabText)
  })

  function verifyActiveTab(page: RenderResult, settingsTabText: string): void {
    const activeNavLink = page.container.getElementsByClassName('active nav-link')

    expect(activeNavLink.length).toBe(1)
    expect(activeNavLink[0].textContent).toBe(settingsTabText)
  }

  async function loadHtmlEntitiesConfig(page: RenderResult): Promise<void> {
    await loadPredefinedConfiguration(page, 0, 'Company summary')
  }

  async function loadPredefinedConfiguration(
    page: RenderResult,
    defaultConfigPosition: number,
    htmlPreviewTitleToExpect: string,
  ): Promise<void> {
    // verify the configuration is not loaded
    expect(screen.queryByText(htmlPreviewTitleToExpect)).not.toBeInTheDocument()

    fireEvent.change(await findByTestId(page.container, ElementId.LOAD_CONFIG_BTN), {
      target: {
        value: JSON.stringify(
          new ConfigurationFactory().getDefaultConfigurations()[defaultConfigPosition],
        ),
      },
    })

    // verify the configuration was loaded
    expect(await findByText(page.container, htmlPreviewTitleToExpect)).toBeVisible()
  }
})
