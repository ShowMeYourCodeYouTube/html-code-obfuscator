import { type ApplicationState } from '../state/applicationState'
import { ConfigurationService } from '../services/configurationService'
import { type PreservedConfiguration } from '../model/configs/preservedConfiguration'

export function saveConfigReducer(
  prevState: ApplicationState,
  userConfig: PreservedConfiguration,
): ApplicationState {
  new ConfigurationService().saveConfig(userConfig)
  return prevState
}
