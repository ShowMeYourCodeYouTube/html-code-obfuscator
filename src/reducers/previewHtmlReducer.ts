import { type ApplicationState } from '../state/applicationState'
import { type HtmlConfig, HtmlGeneratorService } from '../services/htmlGeneratorService'

export function regenerateHtmlReducer(
  prevState: ApplicationState,
  htmlConfig: HtmlConfig,
): ApplicationState {
  const newState = { ...prevState }
  newState.obfuscationConfig.html = new HtmlGeneratorService().generateHTML(htmlConfig)
  return newState
}
