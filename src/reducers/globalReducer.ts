import { type Action } from '../actions/action'
import { ConfigFormActions } from '../actions/configForm'
import {
  algorithmReducer,
  htmlFileTypeReducer,
  htmlTypeFileReducer,
  loadConfigReducer,
  showResultReducer,
} from './configFormReducer'
import { GenerateHtmlModalActions } from '../actions/generateHtmlModalAction'
import { closeModalReducer, generateHtmlReducer } from './generateHtmlModalReducer'
import { PreviewHtmlActions } from '../actions/htmlPreviewActions'
import { regenerateHtmlReducer } from './previewHtmlReducer'
import { ObfuscationOutputActions } from '../actions/obfuscationOutputActions'
import { saveConfigReducer } from './obfuscationOutputReducer'
import { type ApplicationState } from '../state/applicationState'
import { type AlgorithmType } from '../model/algorithms/algorithmType'
import { type HtmlFileType } from '../model/htmlTypes'
import { type PreservedConfiguration } from '../model/configs/preservedConfiguration'
import { type HtmlConfig } from '../services/htmlGeneratorService'

export default class GlobalReducer {
  reduce(currentState: ApplicationState, action: Action): ApplicationState {
    switch (action.type) {
      case ConfigFormActions.SET_ALGORITHM:
        return algorithmReducer(currentState, action.payload as AlgorithmType)
      case ConfigFormActions.SET_HTML_TYPE:
        return htmlFileTypeReducer(currentState, action.payload as HtmlFileType)
      case ConfigFormActions.SET_HTML_FILE:
        return htmlTypeFileReducer(currentState, action.payload as string)
      case GenerateHtmlModalActions.CLOSE_MODAL:
        return closeModalReducer(currentState)
      case GenerateHtmlModalActions.GENERATE_HTML:
        return generateHtmlReducer(currentState, action.payload as HtmlConfig)
      case ConfigFormActions.SHOW_RESULT:
        return showResultReducer(currentState)
      case PreviewHtmlActions.REGENERATE_HTML:
        return regenerateHtmlReducer(currentState, action.payload as HtmlConfig)
      case ObfuscationOutputActions.SAVE_CONFIG:
        return saveConfigReducer(currentState, action.payload as PreservedConfiguration)
      case ConfigFormActions.LOAD_CONFIG:
        return loadConfigReducer(currentState, action.payload as PreservedConfiguration)
      default:
        throw new Error(`No action type ${action.type} implemented!`)
    }
  }
}
