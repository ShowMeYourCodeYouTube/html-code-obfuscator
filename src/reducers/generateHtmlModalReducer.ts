import { type ApplicationState } from '../state/applicationState'
import { type HtmlConfig, HtmlGeneratorService } from '../services/htmlGeneratorService'

export function closeModalReducer(prevState: ApplicationState): ApplicationState {
  const newState = { ...prevState }
  newState.showHtmlTemplateModal = false
  return newState
}

export function generateHtmlReducer(
  prevState: ApplicationState,
  htmlConfig: HtmlConfig,
): ApplicationState {
  const newState = { ...prevState }
  newState.obfuscationConfig.html = new HtmlGeneratorService().generateHTML(htmlConfig)
  newState.obfuscationConfig.htmlConfig = htmlConfig
  newState.showHtmlTemplateModal = false
  return newState
}
