import { HtmlFileType } from '../model/htmlTypes'
import { type ApplicationState } from '../state/applicationState'
import { type AlgorithmType } from '../model/algorithms/algorithmType'
import { type PreservedConfiguration } from '../model/configs/preservedConfiguration'

export function algorithmReducer(
  prevState: ApplicationState,
  valueToUpdate: AlgorithmType,
): ApplicationState {
  const newState = { ...prevState }
  newState.obfuscationConfig.algorithmType = valueToUpdate
  return newState
}

export function htmlFileTypeReducer(
  prevState: ApplicationState,
  valueToUpdate: HtmlFileType,
): ApplicationState {
  const newState = { ...prevState }
  newState.obfuscationConfig.htmlFileType = Number(valueToUpdate)
  if (valueToUpdate === HtmlFileType.GENERATE) {
    newState.showHtmlTemplateModal = true
  }
  return newState
}

export function htmlTypeFileReducer(
  prevState: ApplicationState,
  newContent: string,
): ApplicationState {
  const newState = { ...prevState }
  newState.obfuscationConfig.html = newContent
  return newState
}

export function showResultReducer(prevState: ApplicationState): ApplicationState {
  const newState = { ...prevState }
  newState.activeTab = '3'
  newState.outputObfuscationConfig = { ...newState.obfuscationConfig }
  return newState
}

export function loadConfigReducer(
  prevState: ApplicationState,
  config: PreservedConfiguration,
): ApplicationState {
  const newState = { ...prevState }
  newState.obfuscationConfig = config.config
  return newState
}
