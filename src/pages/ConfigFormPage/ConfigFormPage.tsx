import React, { Component } from 'react'
import { Container, Form } from 'reactstrap'
import GenerateHtmlModalComponent from './components/GenerateHtmlModalComponent/generateHtmlModalComponent'
import { NotificationContainer } from 'react-notifications'
import AlgorithmService from '../../services/algorithmService'
import { type ObfuscationAlgorithm } from '../../model/algorithms/obfuscationAlgorithm'
import { type ObfuscationConfig } from '../../model/configs/obfuscationConfig'
import AlgorithmDetailsViewComponent from './components/AlgorithmDetailsViewComponent/AlgorithmDetailsViewComponent'
import HtmlTypesViewComponent from './components/HtmlTypesViewComponent/HtmlTypesViewComponent'
import AlgorithmSelectViewComponent from './components/AlgorithmSelectViewComponent/AlgorithmSelectViewComponent'
import SubmitBtnViewComponent from './components/SubmitBtnViewComponent/SubmitBtnViewComponent'
import LoadConfigurationViewComponent from './components/LoadConfigurationViewComponent/LoadConfigurationViewComponent'
import { type Action } from '../../actions/action'

export interface ConfigurationFormProps {
  algorithms: ObfuscationAlgorithm[]
  callbackProcessAction: (a: Action) => void
  config: ObfuscationConfig
  showHtmlTemplateModal: boolean
}

class ConfigurationFormPage extends Component<ConfigurationFormProps, Record<string, unknown>> {
  private readonly algorithmService: AlgorithmService
  constructor(props: Readonly<ConfigurationFormProps>) {
    super(props)
    this.algorithmService = new AlgorithmService()
  }

  render(): React.ReactNode {
    const config = this.props.config
    const algorithmHtmlSelectedValue = config.algorithmType ?? 0
    const algorithmDetails = config.algorithmType
      ? this.algorithmService.getAlgorithm(config.algorithmType)
      : undefined
    const htmlSelectValue = config.htmlFileType ?? 0
    return (
      <Container>
        <NotificationContainer />
        <GenerateHtmlModalComponent
          className={''}
          isTemplateModalShown={this.props.showHtmlTemplateModal}
          callbackProcessAction={this.props.callbackProcessAction}
        />
        <Form className='element-m-spacing-t'>
          <LoadConfigurationViewComponent {...this.props} />
          <AlgorithmSelectViewComponent
            algorithmHtmlSelectedValue={algorithmHtmlSelectedValue}
            {...this.props}
          />
          <AlgorithmDetailsViewComponent algorithm={algorithmDetails} />
          <HtmlTypesViewComponent htmlSelectValue={htmlSelectValue} {...this.props} />
          <SubmitBtnViewComponent {...this.props} />
        </Form>
      </Container>
    )
  }
}

export default ConfigurationFormPage
