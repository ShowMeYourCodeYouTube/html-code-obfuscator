import React, { type ChangeEvent, Component } from 'react'
import { Col, FormGroup, Input, Label } from 'reactstrap'
import HtmlTypes, { HtmlFileType } from '../../../../model/htmlTypes'
import { setHtmlFile, setHtmlType } from '../../../../actions/configForm'
import { ElementId } from '../../../../model/elementId'
import { type Action } from '../../../../actions/action'

interface HtmlTypesViewProps {
  callbackProcessAction: (a: Action) => void
  htmlSelectValue: number
}

class HtmlTypesViewComponent extends Component<HtmlTypesViewProps, Record<string, unknown>> {
  render(): React.ReactNode {
    return (
      <FormGroup row className='width-100'>
        <Label sm={2}>HTML to obfuscate</Label>
        <Col sm={10}>
          <Input
            value={this.props.htmlSelectValue}
            type='select'
            onChange={this.handleChangeHtmlType}
            data-testid={ElementId.GENERATE_HTML_OPTION_BTN}
          >
            <option key={0}>&nbsp;</option>
            {this.renderHtmlTypesList()}
          </Input>
        </Col>
        <Col sm={12} className={'margin-m-spacing-t025'}>
          {this.renderFileInput(this.props.htmlSelectValue)}
        </Col>
      </FormGroup>
    )
  }

  private readonly handleChangeHtmlType = (event: ChangeEvent<HTMLInputElement>): void => {
    const newHtmlType = Number(event.target.value)
    this.props.callbackProcessAction(setHtmlType(newHtmlType))
  }

  private readonly handleUserFile = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const fileList = event.target.files
    if (fileList != null) {
      const file = fileList[0]
      const reader = new FileReader()
      reader.onload = (evt: ProgressEvent<FileReader>) => {
        if (evt.target?.result != null) {
          this.props.callbackProcessAction(setHtmlFile(+evt.target.result))
        }
      }
      reader.readAsText(file, 'UTF-8')
    }
  }

  private renderFileInput(htmlSelectValue: number): JSX.Element | null {
    return htmlSelectValue === HtmlFileType.LOAD_FILE ? (
      <Input type='file' label='File' onChange={this.handleUserFile} />
    ) : null
  }

  private readonly renderHtmlTypesList = (): JSX.Element[] => {
    const list = []
    for (const htmlType of HtmlTypes) {
      list.push(
        <option key={htmlType.key} value={htmlType.key}>
          {htmlType.value}
        </option>,
      )
    }
    return list
  }
}

export default HtmlTypesViewComponent
