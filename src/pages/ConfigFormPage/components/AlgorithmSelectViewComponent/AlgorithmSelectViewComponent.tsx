import React, { type ChangeEvent, Component } from 'react'
import { Col, FormGroup, Input, Label } from 'reactstrap'
import { type ObfuscationAlgorithm } from '../../../../model/algorithms/obfuscationAlgorithm'
import { setAlgorithm } from '../../../../actions/configForm'
import { type Action } from '../../../../actions/action'
import { type AlgorithmType } from '../../../../model/algorithms/algorithmType'
import { ElementId } from '../../../../model/elementId'

interface AlgorithmSelectViewProps {
  algorithmHtmlSelectedValue: AlgorithmType
  algorithms: ObfuscationAlgorithm[]
  callbackProcessAction: (a: Action) => void
}

class AlgorithmSelectViewComponent extends Component<
  AlgorithmSelectViewProps,
  Record<string, unknown>
> {
  handleChangeAlgorithms = (event: ChangeEvent<HTMLInputElement>): void => {
    const newChosenAlgorithm = Number(event.target.value)
    this.props.callbackProcessAction(setAlgorithm(newChosenAlgorithm))
  }

  render(): React.ReactNode {
    return (
      <FormGroup row className='width-100 element-m-spacing-t'>
        <Label sm={2}>Algorithm</Label>
        <Col sm={10}>
          <Input
            value={this.props.algorithmHtmlSelectedValue}
            type='select'
            data-testid={ElementId.GENERATE_ALGORITHM_OPTION_BTN}
            onChange={this.handleChangeAlgorithms}
          >
            <option key={0}>&nbsp;</option>
            {this.props.algorithms.map((item: ObfuscationAlgorithm) => (
              <option key={item.type} value={item.type}>
                {item.name}
              </option>
            ))}
          </Input>
        </Col>
      </FormGroup>
    )
  }
}

export default AlgorithmSelectViewComponent
