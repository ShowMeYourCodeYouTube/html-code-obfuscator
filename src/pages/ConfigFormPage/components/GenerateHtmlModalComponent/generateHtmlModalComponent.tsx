import React, { Component, type ChangeEvent } from 'react'
import { Button, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'
import { closeModal, generateHtml } from '../../../../actions/generateHtmlModalAction'
import { HtmlGeneratorService } from '../../../../services/htmlGeneratorService'
import { type Action } from '../../../../actions/action'

export interface GenerateHtmlModalProps {
  callbackProcessAction: (a: Action) => void
  className: string
  isTemplateModalShown: boolean
}

export interface GenerateHtmlModalState {
  headersNumber: number
  inputsNumber: number
  paragraphsNumber: number
  sectionsNumber: number
}

enum HtmlNumberType {
  HEADERS,
  PARAGRAPHS,
  INPUTS,
  SECTIONS,
}

class GenerateHtmlModalComponent extends Component<GenerateHtmlModalProps, GenerateHtmlModalState> {
  constructor(props: GenerateHtmlModalProps) {
    super(props)
    this.state = {
      sectionsNumber: 1,
      headersNumber: 1,
      paragraphsNumber: 1,
      inputsNumber: 1,
    }
  }

  handleGenerationHtmlInputChange(
    event: ChangeEvent<HTMLInputElement>,
    type: HtmlNumberType,
  ): void {
    const newInputValue = +event.target.value
    if (newInputValue >= 0) {
      switch (type) {
        case HtmlNumberType.HEADERS:
          this.setState({
            ...this.state,
            headersNumber: newInputValue,
          })
          break
        case HtmlNumberType.PARAGRAPHS:
          this.setState({
            ...this.state,
            paragraphsNumber: +event.target.value,
          })
          break
        case HtmlNumberType.INPUTS:
          this.setState({
            ...this.state,
            inputsNumber: newInputValue,
          })
          break
        case HtmlNumberType.SECTIONS:
          if (newInputValue > 0) {
            this.setState({
              ...this.state,
              sectionsNumber: newInputValue,
            })
          }
          break
      }
    }
  }

  handleClose = (): void => {
    this.props.callbackProcessAction(closeModal())
  }

  handleGenerate = (): void => {
    const htmlConfig = new HtmlGeneratorService().prepareHtmlConfig(
      this.state.sectionsNumber,
      this.state.headersNumber,
      this.state.paragraphsNumber,
      this.state.inputsNumber,
    )
    this.props.callbackProcessAction(generateHtml(htmlConfig))
  }

  render(): React.ReactNode {
    const className = this.props.className
    return (
      <Modal isOpen={this.props.isTemplateModalShown} className={className}>
        <ModalHeader>Generate HTML to obfuscate</ModalHeader>
        <ModalBody>
          <p>Choose HTML elements for generated HTML.</p>
          <Label>Sections</Label>
          <Input
            type='number'
            data-testid='Sections'
            value={this.state.sectionsNumber}
            onChange={(e) => {
              this.handleGenerationHtmlInputChange(e, HtmlNumberType.SECTIONS)
            }}
          />
          <Label>Headers</Label>
          <Input
            type='number'
            data-testid='Headers'
            value={this.state.headersNumber}
            onChange={(e) => {
              this.handleGenerationHtmlInputChange(e, HtmlNumberType.HEADERS)
            }}
          />
          <Label>Paragraphs</Label>
          <Input
            type='number'
            data-testid='Paragraphs'
            value={this.state.paragraphsNumber}
            onChange={(e) => {
              this.handleGenerationHtmlInputChange(e, HtmlNumberType.PARAGRAPHS)
            }}
          />
          <Label>Inputs</Label>
          <Input
            type='number'
            data-testid='Inputs'
            value={this.state.inputsNumber}
            onChange={(e) => {
              this.handleGenerationHtmlInputChange(e, HtmlNumberType.INPUTS)
            }}
          />
        </ModalBody>
        <ModalFooter>
          <Button color='primary' onClick={this.handleGenerate}>
            Generate
          </Button>{' '}
          <Button color='secondary' onClick={this.handleClose}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    )
  }
}

export default GenerateHtmlModalComponent
