import React, { Component } from 'react'
import { Button, Col, Collapse, FormGroup, ListGroup, ListGroupItem } from 'reactstrap'
import { type ObfuscationAlgorithm } from '../../../../model/algorithms/obfuscationAlgorithm'

interface AlgorithmDetailsViewProps {
  algorithm: ObfuscationAlgorithm | undefined
}

interface AlgorithmDetailsViewState {
  showDetails: boolean
}

class AlgorithmDetailsViewComponent extends Component<
  AlgorithmDetailsViewProps,
  AlgorithmDetailsViewState
> {
  constructor(props: Readonly<AlgorithmDetailsViewProps>) {
    super(props)
    this.state = {
      showDetails: false,
    }
  }

  openCloseDetails = (): void => {
    this.setState({
      showDetails: !this.state.showDetails,
    })
  }

  render(): React.ReactNode {
    return this.props.algorithm !== undefined ? this.renderDetailsElement() : null
  }

  private renderDetailsElement(): JSX.Element {
    return (
      <FormGroup row className='width-100'>
        <Col sm={{ size: 2, offset: 10 }}>
          <Button color='primary' onClick={this.openCloseDetails} className='element-m-spacing-b'>
            Show details
          </Button>
        </Col>
        <Collapse isOpen={this.state.showDetails} className='width-100'>
          <ListGroup flush>
            {this.props.algorithm?.details.steps.map((step: string, index: number) => (
              <ListGroupItem key={index}>
                {index + 1}. {step}
              </ListGroupItem>
            ))}
          </ListGroup>
        </Collapse>
      </FormGroup>
    )
  }
}

export default AlgorithmDetailsViewComponent
