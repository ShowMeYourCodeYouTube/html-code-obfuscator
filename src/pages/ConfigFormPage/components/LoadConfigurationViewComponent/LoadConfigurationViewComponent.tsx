import React, { Component } from 'react'
import { Col, FormGroup, Input } from 'reactstrap'
import ConfigurationFactory from '../../../../factories/configurationFactory'
import { loadConfigAction } from '../../../../actions/configForm'
import { NotificationManager } from 'react-notifications'
import { ConfigurationService } from '../../../../services/configurationService'
import { ElementId } from '../../../../model/elementId'
import { type Action } from '../../../../actions/action'
import { type PreservedConfiguration } from '../../../../model/configs/preservedConfiguration'

interface LoadConfigurationViewProps {
  callbackProcessAction: (a: Action) => void
}

class LoadConfigurationViewComponent extends Component<
  LoadConfigurationViewProps,
  Record<string, unknown>
> {
  private readonly configurationFactory: ConfigurationFactory
  private readonly configurationService: ConfigurationService

  constructor(props: LoadConfigurationViewProps) {
    super(props)
    this.configurationFactory = new ConfigurationFactory()
    this.configurationService = new ConfigurationService()
  }

  handleLoadConfig = (event: React.ChangeEvent<HTMLInputElement>): void => {
    if (event.target.value.trim().length > 0) {
      const preservedConfig = JSON.parse(event.target.value) as PreservedConfiguration
      this.props.callbackProcessAction(loadConfigAction(preservedConfig))
      NotificationManager.success('Successfully loaded the config!', preservedConfig.name)
    }
  }

  render(): React.ReactNode {
    return (
      <FormGroup row className='width-100'>
        <Col sm={9}>Load a configuration</Col>
        <Col sm={3}>
          <Input
            type='select'
            data-testid={ElementId.LOAD_CONFIG_BTN}
            onChange={this.handleLoadConfig}
          >
            <option key={0}>&nbsp;</option>
            <option key={1} disabled={true}>
              default
            </option>
            {this.configurationFactory.getDefaultConfigurations().map((elem, idx) => (
              <option key={elem.name} value={JSON.stringify(elem)}>
                {elem.name}
              </option>
            ))}
            <option key={2} disabled={true}>
              saved
            </option>
            {this.configurationService.loadConfigs().map((elem) => (
              <option key={elem.name} value={JSON.stringify(elem)}>
                {elem.name}
              </option>
            ))}
          </Input>
        </Col>
      </FormGroup>
    )
  }
}

export default LoadConfigurationViewComponent
