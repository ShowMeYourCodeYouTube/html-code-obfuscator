import React, { Component } from 'react'
import { Button, Col, FormGroup } from 'reactstrap'
import ConfigurationFactory from '../../../../factories/configurationFactory'
import { showResult } from '../../../../actions/configForm'
import { type ObfuscationConfig } from '../../../../model/configs/obfuscationConfig'
import { ElementId } from '../../../../model/elementId'
import { type Action } from '../../../../actions/action'

interface SubmitBtnViewProps {
  callbackProcessAction: (a: Action) => void
  config: ObfuscationConfig
}

class SubmitBtnViewComponent extends Component<SubmitBtnViewProps, Record<string, unknown>> {
  private readonly configurationFactory: ConfigurationFactory

  constructor(props: SubmitBtnViewProps) {
    super(props)
    this.configurationFactory = new ConfigurationFactory()
  }

  handleSubmit = (): void => {
    this.props.callbackProcessAction(showResult())
  }

  render(): React.ReactNode {
    return (
      <FormGroup row className='width-100'>
        <Col sm={{ size: 2, offset: 10 }}>
          <Button
            type='button'
            data-testid={ElementId.GENERATE_SUBMIT_BTN}
            disabled={!this.configurationFactory.isConfigurationComplete(this.props.config)}
            className='btn btn-success'
            onClick={this.handleSubmit}
          >
            Submit
          </Button>
        </Col>
      </FormGroup>
    )
  }
}

export default SubmitBtnViewComponent
