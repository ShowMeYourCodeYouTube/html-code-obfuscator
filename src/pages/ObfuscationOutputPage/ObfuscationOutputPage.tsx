import React, { type ChangeEvent, Component } from 'react'
import { Button, Col, Container, Input, Row } from 'reactstrap'
import { saveConfigAction } from '../../actions/obfuscationOutputActions'
import { NotificationContainer, NotificationManager } from 'react-notifications'
import { AlgorithmType } from '../../model/algorithms/algorithmType'
import HtmlEncoderService from '../../services/htmlEncoderService'
import ConfigurationFactory from '../../factories/configurationFactory'
import { type ObfuscationConfig } from '../../model/configs/obfuscationConfig'
import './ObfuscationOutputPage.scss'
import { ConfigurationService } from '../../services/configurationService'
import { LOGGER } from '../../utils/logging'
import { type Action } from '../../actions/action'
import prettify from 'html-prettify'
import { ElementId } from '../../model/elementId'

export interface ObfuscationOutputProps {
  callbackProcessAction: (a: Action) => void
  config?: ObfuscationConfig
}

export interface ObfuscationOutputState {
  config?: ObfuscationConfig
  configToSaveName: string
  html: string
  readyToEdit: boolean
  result: string
}

class ObfuscationOutputPage extends Component<ObfuscationOutputProps, ObfuscationOutputState> {
  private readonly htmlEncoderService: HtmlEncoderService
  private readonly configurationFactory: ConfigurationFactory
  private readonly configurationService: ConfigurationService

  constructor(props: ObfuscationOutputProps) {
    super(props)
    this.state = {
      html: '',
      result: '',
      readyToEdit: false,
      configToSaveName: '',
    }
    this.htmlEncoderService = new HtmlEncoderService()
    this.configurationFactory = new ConfigurationFactory()
    this.configurationService = new ConfigurationService()
  }

  componentDidUpdate(
    prevProps: Readonly<ObfuscationOutputProps>,
    prevState: Readonly<ObfuscationOutputState>,
  ): void {
    if (
      this.props.config !== undefined &&
      this.isConfigurationChanged(this.state.config, this.props.config)
    ) {
      this.setState({
        ...this.state,
        html: prettify(this.props.config.html),
        result: this.processHtml(this.props.config.html, this.props.config.algorithmType as number),
        config: this.props.config,
      })
    }
  }

  UNSAFE_componentWillReceiveProps(props: Readonly<ObfuscationOutputProps>): void {
    if (this.state.config !== undefined && props.config !== undefined) {
      this.setState({
        ...this.state,
        config: this.props.config,
        result: this.processHtml(
          (this.props.config as ObfuscationConfig).html,
          props.config.algorithmType,
        ),
      })
    }
  }

  handleChange = (evt: ChangeEvent<HTMLInputElement>): void => {
    const changedHtml = evt.target.value
    this.setState({
      ...this.state,
      result: this.processHtml(changedHtml, this.props.config?.algorithmType as number),
      html: changedHtml,
    })
  }

  isConfigurationChanged(oldConfig?: ObfuscationConfig, newConfig?: ObfuscationConfig): boolean {
    return oldConfig?.algorithmType !== newConfig?.algorithmType
  }

  processHtml(html: string, algorithmType?: AlgorithmType): string {
    let result = ''
    switch (algorithmType) {
      case AlgorithmType.HTML_TO_JAVASCRIPT: {
        const htmlToJs = this.htmlEncoderService.htmlToJavascript(html)
        result = `document.write(eval('${htmlToJs}'))`
        break
      }
      case AlgorithmType.HTML_TO_BASE64: {
        const encodedHtml = btoa(escape(html))
        result = `document.write(unescape(atob('${encodedHtml}')) `
        break
      }
      case AlgorithmType.HTML_TO_HEX: {
        const hex = this.htmlEncoderService.toHex(html)
        result = `document.write(fromHex('${hex}'))`
        break
      }
      case AlgorithmType.HTML_TO_HTML_ENTITIES: {
        const ascii = this.htmlEncoderService.toHtmlEntities(html)
        result = `document.write(fromHtmlEntities('${ascii}'))`
        break
      }
      case AlgorithmType.HTML_ESCAPE_CHARACTERS: {
        const escapedHtml = escape(html)
        result = `document.write(unescape('${escapedHtml}'))`
        break
      }
      case AlgorithmType.HTML_ENCODE_WITH_OWN_FUN: {
        const customEncoding = this.htmlEncoderService.encodeUsingOwnFunction(html)
        result = `document.write(ownDecodingFunction('${customEncoding}'))`
        break
      }
      default:
        LOGGER.error('Not implemented method for an obfuscation algorithm!')
    }
    return result
  }

  render(): React.ReactNode {
    return (
      <div>
        <NotificationContainer />
        <Container>
          <Row className='element-m-spacing-t'>
            <Col sm={6}></Col>
            <Col sm={4}>
              <Input
                type='text'
                data-testid={ElementId.RESULT_SAVE_CONFIG}
                placeholder='Configuration name'
                value={this.state.configToSaveName}
                onChange={this.updateConfigToSaveName}
              />
            </Col>
            <Col sm={2}>
              <Button
                type='button'
                className='btn btn-info'
                onClick={this.saveConfiguration}
                disabled={this.state.configToSaveName.length === 0}
              >
                Save configuration
              </Button>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <h4>HTML</h4>
            </Col>
            <Col sm={6}>
              <h4>Javascript</h4>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <Input
                value={this.state.html}
                type='textarea'
                className='output-textarea'
                onChange={this.handleChange}
              />
            </Col>
            <Col sm={6} className='output-text'>
              {this.state.result}
            </Col>
          </Row>
        </Container>
      </div>
    )
  }

  saveConfiguration = (): void => {
    if (this.configurationService.checkIfConfigExists(this.state.configToSaveName)) {
      NotificationManager.error('Configuration already exists.')
    }
    if (this.state.config === undefined) {
      LOGGER.error('Configuration is undefined!')
      NotificationManager.error('Cannot save undefined configuration.')
    } else {
      this.props.callbackProcessAction(
        saveConfigAction(
          this.configurationFactory.createUserConfig(
            this.state.configToSaveName,
            this.state.config,
          ),
        ),
      )
      NotificationManager.info('Configuration saved!')
      this.setState({
        ...this.state,
        configToSaveName: '',
      })
    }
  }

  updateConfigToSaveName = (evt: ChangeEvent<HTMLInputElement>): void => {
    this.setState({
      ...this.state,
      configToSaveName: evt.target.value,
    })
  }
}

export default ObfuscationOutputPage
