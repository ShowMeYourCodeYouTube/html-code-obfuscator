import React, { Component, type RefObject } from 'react'
import { Button } from 'reactstrap'
import { regenerateHtml } from '../../actions/htmlPreviewActions'
import './HtmlPreviewPage.scss'
import { type HtmlConfig } from '../../services/htmlGeneratorService'
import { type Action } from '../../actions/action'

export interface HtmlPreviewProps {
  callbackProcessAction: (a: Action) => void
  htmlConfig?: HtmlConfig
  previewHtml?: string
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface HtmlPreviewState {}

class HtmlPreviewPage extends Component<HtmlPreviewProps, HtmlPreviewState> {
  private readonly previewRef: RefObject<HTMLDivElement> = React.createRef()
  constructor(props: HtmlPreviewProps) {
    super(props)
    this.state = {}
  }

  componentDidUpdate(): void {
    if (this.props.previewHtml !== undefined && this.props.previewHtml !== '') {
      ;(this.previewRef.current as HTMLDivElement).innerHTML = this.props.previewHtml
    }
  }

  generateAgain = (): void => {
    this.props.callbackProcessAction(regenerateHtml(this.props.htmlConfig as HtmlConfig))
  }

  render(): React.ReactNode {
    return (
      <div className='preview-wrapper'>
        {this.props.htmlConfig !== null ? (
          <Button className='preview-generate-btn' color='primary' onClick={this.generateAgain}>
            Generate again
          </Button>
        ) : null}
        <div ref={this.previewRef}>...</div>
      </div>
    )
  }
}

export default HtmlPreviewPage
