import React, { Component } from 'react'
import { Button, Col, Container, Label, Row } from 'reactstrap'
import { ConfigurationService } from '../../services/configurationService'

class SettingsTabPage extends Component<Record<string, unknown>, Record<string, unknown>> {
  private readonly configurationService: ConfigurationService

  constructor(props: Record<string, unknown>) {
    super(props)
    this.configurationService = new ConfigurationService()
  }

  clearConfigurations = (): void => {
    this.configurationService.clearUserConfigs()
  }

  render(): React.ReactNode {
    return (
      <Container className='element-m-spacing-t'>
        <Row>
          <Label sm={10}>Clear saved configurations</Label>
          <Col sm={2}>
            <Button color='danger' onClick={this.clearConfigurations}>
              Clear
            </Button>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default SettingsTabPage
