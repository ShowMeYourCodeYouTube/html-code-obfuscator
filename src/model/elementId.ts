export namespace ElementId {
  export const LOAD_CONFIG_BTN = 'load-config-btn'
  export const GENERATE_HTML_OPTION_BTN = 'generate-html-option-btn'
  export const GENERATE_ALGORITHM_OPTION_BTN = 'generate-algorithm-option-btn'
  export const GENERATE_SUBMIT_BTN = 'generate-html-submit-btn'
  export const RESULT_SAVE_CONFIG = 'result-save-config-btn'
}
