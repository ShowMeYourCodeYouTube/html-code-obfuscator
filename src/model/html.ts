import { type HtmlConfig } from '../services/htmlGeneratorService'

export interface HtmlSection {
  html: string
  config: HtmlConfig
}
