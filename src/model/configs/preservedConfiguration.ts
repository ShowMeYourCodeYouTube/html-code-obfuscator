import { type ObfuscationConfig } from './obfuscationConfig'

export interface PreservedConfiguration {
  config: ObfuscationConfig
  name: string
}
