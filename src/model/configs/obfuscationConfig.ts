import { type AlgorithmType } from '../algorithms/algorithmType'
import { type HtmlFileType } from '../htmlTypes'
import { type HtmlConfig } from '../../services/htmlGeneratorService'

export const EMPTY_HTML = ''
export interface ObfuscationConfig {
  algorithmType?: AlgorithmType
  html: string
  htmlConfig?: HtmlConfig
  htmlFileType?: HtmlFileType
}
