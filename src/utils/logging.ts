import * as log from 'loglevel'
import { type LogLevelDesc } from 'loglevel'

const { REACT_APP_LOG_LEVEL } = process.env

log.setLevel(REACT_APP_LOG_LEVEL as LogLevelDesc)

export const LOGGER = log
