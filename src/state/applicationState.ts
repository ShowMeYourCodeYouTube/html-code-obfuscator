import { type ObfuscationConfig } from '../model/configs/obfuscationConfig'
import { type ObfuscationAlgorithm } from '../model/algorithms/obfuscationAlgorithm'
import AlgorithmService from '../services/algorithmService'
import { ConfigurationService } from '../services/configurationService'

export interface ApplicationState {
  activeTab: string
  algorithms: ObfuscationAlgorithm[]
  obfuscationConfig: ObfuscationConfig
  outputObfuscationConfig?: ObfuscationConfig
  showHtmlTemplateModal: boolean
}

export function getInitialState(): ApplicationState {
  return {
    algorithms: new AlgorithmService().getDefaultAlgorithms(),
    obfuscationConfig: new ConfigurationService().getInitialObfuscationConfig(),
    activeTab: '1',
    showHtmlTemplateModal: false,
    outputObfuscationConfig: undefined,
  }
}
