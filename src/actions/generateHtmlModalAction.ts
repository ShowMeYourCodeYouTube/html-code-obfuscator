import { type Action, type ActionPayload } from './action'

export const closeModal: () => Action = () => {
  return {
    type: GenerateHtmlModalActions.CLOSE_MODAL,
    payload: '',
  }
}

export const generateHtml: (_: ActionPayload) => Action = (htmlConfig: ActionPayload) => {
  return {
    type: GenerateHtmlModalActions.GENERATE_HTML,
    payload: htmlConfig,
  }
}

export const GenerateHtmlModalActions = {
  CLOSE_MODAL: 'CLOSE_MODAL',
  GENERATE_HTML: 'GENERATE_HTML',
}
