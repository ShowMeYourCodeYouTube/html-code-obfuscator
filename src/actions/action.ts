import { type HtmlFileType } from '../model/htmlTypes'
import { type AlgorithmType } from '../model/algorithms/algorithmType'
import { type PreservedConfiguration } from '../model/configs/preservedConfiguration'
import { type HtmlConfig } from '../services/htmlGeneratorService'
import { type ObfuscationConfig } from '../model/configs/obfuscationConfig'

export type ActionPayload =
  | string
  | ObfuscationConfig
  | HtmlFileType
  | AlgorithmType
  | PreservedConfiguration
  | HtmlConfig

export interface Action {
  payload: ActionPayload
  type: string
}
