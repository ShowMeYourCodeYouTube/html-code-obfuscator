import { type Action } from './action'
import { type PreservedConfiguration } from '../model/configs/preservedConfiguration'

export function saveConfigAction(config: PreservedConfiguration): Action {
  return {
    type: ObfuscationOutputActions.SAVE_CONFIG,
    payload: config,
  }
}

export const ObfuscationOutputActions = {
  SAVE_CONFIG: 'SAVE_CONFIG',
}
