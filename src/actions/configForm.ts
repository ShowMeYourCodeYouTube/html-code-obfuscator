import { type Action, type ActionPayload } from './action'
import { type HtmlFileType } from '../model/htmlTypes'
import { type AlgorithmType } from '../model/algorithms/algorithmType'

export const setAlgorithm: (_: AlgorithmType) => Action = (algorithmId: AlgorithmType) => {
  return {
    type: ConfigFormActions.SET_ALGORITHM,
    payload: algorithmId,
  }
}

export const setHtmlType: (_: ActionPayload) => Action = (fileType: ActionPayload) => {
  return {
    type: ConfigFormActions.SET_HTML_TYPE,
    payload: fileType,
  }
}

export const setHtmlFile: (_: HtmlFileType) => Action = (file: ActionPayload) => {
  return {
    type: ConfigFormActions.SET_HTML_FILE,
    payload: file,
  }
}

export const showResult: () => Action = () => {
  return {
    type: ConfigFormActions.SHOW_RESULT,
    payload: '',
  }
}

export const loadConfigAction: (_: ActionPayload) => Action = (config: ActionPayload) => {
  return {
    type: ConfigFormActions.LOAD_CONFIG,
    payload: config,
  }
}

export const ConfigFormActions = {
  SET_ALGORITHM: 'SET_ALGORITHM',
  SET_HTML_TYPE: 'SET_HTML_TYPE',
  SET_HTML_FILE: 'SET_HTML_FILE',
  SHOW_GENERATE_HTML_MODAL: 'SHOW_GENERATE_HTML_MODAL',
  SHOW_RESULT: 'SHOW_RESULT',
  LOAD_CONFIG: 'LOAD_CONFIG',
}
