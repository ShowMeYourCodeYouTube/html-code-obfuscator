import { type Action, type ActionPayload } from './action'

export function regenerateHtml(config: ActionPayload): Action {
  return {
    type: PreviewHtmlActions.REGENERATE_HTML,
    payload: config,
  }
}

export const PreviewHtmlActions = {
  REGENERATE_HTML: 'REGENERATE_HTML',
}
