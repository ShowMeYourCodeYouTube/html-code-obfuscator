import { type ObfuscationConfig } from '../model/configs/obfuscationConfig'
import { type PreservedConfiguration } from '../model/configs/preservedConfiguration'
import { AlgorithmType } from '../model/algorithms/algorithmType'
import { HtmlFileType } from '../model/htmlTypes'
import { FileName, loadTemplate } from '../utils/fileLoader'

export default class ConfigurationFactory {
  createConfig(
    algorithm: AlgorithmType,
    htmlFileType: HtmlFileType,
    html: string,
  ): ObfuscationConfig {
    return {
      algorithmType: algorithm,
      html,
      htmlConfig: {
        header: 1,
        input: 3,
        paragraph: 2,
        section: 3,
      },
      htmlFileType,
    }
  }

  createPreservedConfiguration(name: string, config: ObfuscationConfig): PreservedConfiguration {
    return {
      name,
      config,
    }
  }

  createUserConfig(name: string, config: ObfuscationConfig): PreservedConfiguration {
    return this.createPreservedConfiguration(name, config)
  }

  getDefaultConfigurations(): PreservedConfiguration[] {
    return [
      this.createPreservedConfiguration(
        'HTML entities',
        this.createConfig(
          AlgorithmType.HTML_TO_HTML_ENTITIES,
          HtmlFileType.LOAD_FILE,
          loadTemplate(FileName.EXAMPLE1),
        ),
      ),
      this.createPreservedConfiguration(
        'Base64',
        this.createConfig(
          AlgorithmType.HTML_TO_BASE64,
          HtmlFileType.LOAD_FILE,
          loadTemplate(FileName.EXAMPLE2),
        ),
      ),
    ]
  }

  isConfigurationComplete(config: ObfuscationConfig): boolean {
    return config?.algorithmType !== undefined && config.htmlFileType !== undefined
  }
}
