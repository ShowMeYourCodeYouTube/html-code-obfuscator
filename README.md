# HTML Obfuscator

- Live demo: https://html-code-obfuscator.netlify.app/
- Test report: https://showmeyourcodeyoutube.gitlab.io/html-code-obfuscator/unit-tests/index.html

---

![Demo](./docs/demo.gif)

A project based on ReactJS and Redux (any external library wasn't used). It aims to show the idea of code obfuscation but in a really simple way. You don't find here complicated algorithms or implementations of clever ideas.

```
The main idea of code obfuscation is to make the source code difficult to read and understand by humans. 
It is useful when you want to hide some implementation details which may contain clever ideas.
```

https://searchsoftwarequality.techtarget.com/definition/obfuscation

## Technology stack

- React.js
- Typescript
- Bootstrap + reactstrap

## Application flow

![Redux](./docs/redux.png "Redux concept")

## Add Typescript to existing React project

https://facebook.github.io/create-react-app/docs/adding-typescript

1. npm install --save typescript @types/node @types/react @types/react-dom @types/jest
2. install types for external libraries! for example "@types/reactstrap"
    - if the library does not have types, create custom module with types for partciular library (see modules package)
3. Create properties/states for components according to Component interface (react.Component<P,S>)
4. Changes other file extensions from .js to tsx (optional)

---
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## SonarQube

https://docs.sonarqube.org/latest/user-guide/metric-definitions/
