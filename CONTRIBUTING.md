# CONTRIBUTING

- Follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).
  - [Semantic Commit Messages](https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716)
- Follow the naming convention below.
  - Any component should be PascalCase e.g. `MyComponent.tsx`.
  - Styles related to a particular component should follow the component name with `scss` extension.
  - Files contain model definitions or variables should be camelCase.
